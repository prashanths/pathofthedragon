"use strict";
// https://cf.geekdo-images.com/images/pic2082742_md.png
var TILECONFIGS = [
    { 1: 2, 3: 4, 5: 6, 7: 8 },
    { 1: 2, 3: 4, 5: 7, 6: 8 },
    { 1: 2, 3: 4, 5: 8, 6: 7 },
    { 1: 2, 3: 5, 4: 7, 6: 8 },
    { 1: 2, 3: 5, 4: 8, 6: 7 },
    { 1: 2, 3: 7, 4: 5, 6: 8 },
    { 1: 2, 3: 8, 4: 5, 6: 7 },
    { 1: 2, 3: 6, 4: 7, 5: 8 },
    { 1: 2, 3: 6, 4: 8, 5: 7 },
    { 1: 2, 3: 7, 4: 6, 5: 8 },
    { 1: 2, 3: 8, 4: 6, 5: 7 },
    { 1: 2, 3: 7, 4: 8, 5: 6 },
    { 1: 2, 3: 8, 4: 7, 5: 6 },
    { 1: 3, 2: 4, 5: 7, 6: 8 },
    { 1: 3, 2: 4, 5: 8, 6: 7 },
    { 1: 3, 2: 5, 4: 7, 6: 8 },
    { 1: 3, 2: 5, 4: 8, 6: 7 },
    { 1: 3, 2: 7, 4: 5, 6: 8 },
    { 1: 3, 2: 8, 4: 5, 6: 7 },
    { 1: 3, 2: 6, 4: 7, 5: 8 },
    { 1: 3, 2: 6, 4: 8, 5: 7 },
    { 1: 3, 2: 7, 4: 6, 5: 8 },
    { 1: 3, 2: 8, 4: 6, 5: 7 },
    { 1: 4, 2: 3, 5: 8, 6: 7 },
    { 1: 5, 2: 3, 4: 7, 6: 8 },
    { 1: 5, 2: 3, 4: 8, 6: 7 },
    { 1: 8, 2: 3, 4: 5, 6: 7 },
    { 1: 6, 2: 3, 4: 7, 5: 8 },
    { 1: 4, 2: 5, 3: 7, 6: 8 },
    { 1: 4, 2: 5, 3: 7, 6: 8 },
    { 1: 4, 2: 7, 3: 6, 5: 8 },
    { 1: 5, 2: 4, 3: 7, 6: 8 },
    { 1: 5, 2: 6, 3: 7, 4: 8 },
    { 1: 5, 2: 6, 3: 8, 4: 7 },
    { 1: 6, 2: 5, 3: 8, 4: 7 } // 5
];
var shuffle = function (o) {
    for (var j, x, i = o.length; i; j = Math.floor(Math.random() * i), x = o[--i], o[i] = o[j], o[j] = x)
        ;
    return o;
};
/*
    ____________
    |  1    2  |
    |8        3|
    |          |
    |7        4|
    |__6____5__|

*/
var Tile = (function () {
    function Tile(board, paths, rotation) {
        // map of paths 1=>3, etc.
        this.paths = {};
        this.board = board;
        this.paths = paths;
        this.rotation = rotation;
    }
    // get paths with rotation applied...
    Tile.prototype.getPaths = function () {
        var newPaths = {};
        for (var from in this.paths) {
            var to = this.paths[from];
            var newFrom = parseInt(from) + (2 * this.rotation), newTo = parseInt(to) + (2 * this.rotation);
            if (newFrom > 8)
                newFrom = newFrom % 8;
            if (newTo > 8)
                newTo = newTo % 8;
            newPaths[newFrom] = newTo;
        }
        return newPaths;
    };
    Tile.prototype.getDestination = function (entry) {
        var paths = this.getPaths();
        if (paths[entry] != undefined)
            return paths[entry];
        // pretty much indexOf()
        for (var from in paths) {
            if (paths[from] == entry)
                return parseInt(from);
        }
        throw Error('Invalid request for entry ' + entry + '. Cannot find destination.');
    };
    Tile.prototype.rotate = function () {
        this.rotation = (this.rotation + 1) % 4;
    };
    return Tile;
})();
var Board = (function () {
    function Board() {
        this.initialize(2);
    }
    // start a new game...
    Board.prototype.initialize = function (players) {
        this.gameOver = false;
        // setup the stack...
        this.stack = [];
        var tileClones = TILECONFIGS.slice();
        for (var i = 0; i < 35; i++) {
            var tile = new Tile(this, tileClones.pop(), 0);
            this.stack.push(tile);
        }
        this.stack = shuffle(this.stack);
        // setup players...
        this.playerTurn = 0;
        this.players = [];
        for (var i = 0; i < players; i++) {
            var hand = [];
            for (var n = 0; n < 3; n++) {
                hand.push(this.stack.pop());
            }
            var rand = Math.floor(Math.random() * 6), rand2 = Math.random() > 0.5 ? -1 : 6;
            if (Math.random() > 0.5) {
                var player = new Player(this, hand, { 'x': rand2, 'y': rand, 'pathNumber': 1 });
            }
            else {
                var player = new Player(this, hand, { 'x': rand, 'y': rand2, 'pathNumber': 1 });
            }
            this.players.push(player);
        }
        // setup the board's empty tiles...
        this.board = [];
        for (var x = 0; x < 6; x++) {
            this.board[x] = [];
            for (var y = 0; y < 6; y++) {
                this.board[x][y] = null;
            }
        }
    };
    Board.prototype.getCurrentPlayer = function () {
        return this.players[this.playerTurn];
    };
    Board.prototype.takeFromStack = function () {
        return this.stack.pop();
    };
    Board.prototype.place = function (handIdx) {
        var player = this.getCurrentPlayer();
        var tile = player.hand[handIdx];
        player.hand.splice(handIdx, 1);
        var nextPos = player.getPlaceableSpot();
        this.board[nextPos[0]][nextPos[1]] = tile;
        this.movePlayers();
        if (!player.isLoser())
            player.pickTile();
        this.playerTurn = (this.playerTurn + 1) % this.players.length;
        while (this.getCurrentPlayer().isLoser()) {
            // if we rotate around to our current player...
            if (player == this.getCurrentPlayer())
                break;
            this.playerTurn = (this.playerTurn + 1) % this.players.length;
        }
    };
    // move each player to the end of the road...
    Board.prototype.movePlayers = function () {
        for (var idx in this.players) {
            this.players[idx].followPath();
        }
    };
    Board.prototype.isGameOver = function () {
        var playersAvailable = this.players.length;
        for (var idx in this.players) {
            if (this.players[idx].atEdge())
                playersAvailable--;
        }
        return playersAvailable < 2;
    };
    return Board;
})();
var Player = (function () {
    function Player(board, hand, position) {
        this.hand = [];
        // x, y, path
        this.position = {
            'x': null,
            'y': null,
            'pathNumber': null,
        };
        this.board = board;
        this.hand = hand;
        this.position.x = position.x;
        this.position.y = position.y;
        this.position.pathNumber = position.pathNumber;
    }
    Player.prototype.isMyTurn = function () {
        return this.board.getCurrentPlayer() == this;
    };
    Player.prototype.pickTile = function () {
        if (this.hand.length >= 3)
            throw new Error('Cannot have more than 3 cards in your hand.');
        this.hand.push(this.board.takeFromStack());
    };
    // returns x and y coords of the next placable spot...
    Player.prototype.getPlaceableSpot = function () {
        if (this.atEdge())
            return null;
        var pos = this.position;
        // find out the position we can move to...
        if (pos.x == -1) {
            return [pos.x + 1, pos.y];
        }
        if (pos.x == 6) {
            return [pos.x - 1, pos.y];
        }
        if (pos.y == -1) {
            return [pos.x, pos.y + 1];
        }
        if (pos.y == 6) {
            return [pos.x, pos.y - 1];
        }
        switch (pos.pathNumber) {
            case 1:
            case 2:
                return [pos.x, pos.y - 1];
            case 3:
            case 4:
                return [pos.x + 1, pos.y];
            case 5:
            case 6:
                return [pos.x, pos.y + 1];
            case 7:
            case 8:
                return [pos.x - 1, pos.y];
        }
        return null;
    };
    Player.prototype.atEdge = function () {
        // if we are standing on the edges...
        if (this.position.x == -1 || this.position.x == 6 || this.position.y == -1 || this.position.y == 6)
            return false;
        if (this.position.x == 0 && (this.position.pathNumber == 7 || this.position.pathNumber == 8))
            return true;
        if (this.position.x == 5 && (this.position.pathNumber == 3 || this.position.pathNumber == 4))
            return true;
        if (this.position.y == 0 && (this.position.pathNumber == 1 || this.position.pathNumber == 2))
            return true;
        if (this.position.y == 5 && (this.position.pathNumber == 5 || this.position.pathNumber == 6))
            return true;
        return false;
    };
    Player.prototype.followPath = function () {
        while (true) {
            var spot = this.getPlaceableSpot(), pos = this.position;
            // null is returned if we are at the edge...
            if (spot === null)
                return;
            // get the tile at that spot...
            var tileTo = this.board.board[spot[0]][spot[1]];
            // if no tile we are done
            if (tileTo === null)
                return;
            var from = pos.pathNumber;
            if (pos.x == -1) {
                from = from == 1 ? 3 : 4;
            }
            else if (pos.x == 6) {
                from = from == 1 ? 8 : 7;
            }
            else if (pos.y == -1) {
                from = from == 1 ? 6 : 5;
            }
            else if (pos.y == 6) {
                from = from == 1 ? 2 : 1;
            }
            var entry = null;
            // convert from to entry point...
            if (from == 1 || from == 3)
                entry = from + 5;
            if (from == 2 || from == 4)
                entry = from + 3;
            if (from == 6 || from == 8)
                entry = from - 5;
            if (from == 5 || from == 7)
                entry = from - 3;
            var to = tileTo.getDestination(entry);
            this.position.x = spot[0];
            this.position.y = spot[1];
            this.position.pathNumber = to;
        }
    };
    Player.prototype.isLoser = function () {
        return this.atEdge();
    };
    return Player;
})();
var game = new Board;
